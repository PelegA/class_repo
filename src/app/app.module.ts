import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
// import { UsersComponent } from './users/users.component';
// import { UsersService} from './users/users.service';
// import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
// import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
// import { UserFormComponent } from './user-form/user-form.component';
import { AngularFireModule } from 'angularfire2';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { InvoicesService } from './invoices/invoices.service';

export const firebaseConfig={
    apiKey: "AIzaSyAuvjPKP-JT76Wi2k5LVh66KOOyPamaoiU",
    authDomain: "angularclass-5ef23.firebaseapp.com",
    databaseURL: "https://angularclass-5ef23.firebaseio.com",
    storageBucket: "angularclass-5ef23.appspot.com",
    messagingSenderId: "1053283773735"
}

const appRoutes:Routes= [
   {path: 'invoice-form', component:InvoiceFormComponent},
   {path: 'invoices', component:InvoicesComponent},
   {path: '', component:InvoicesComponent},
  {path: '**', component:PageNotFoundComponent},
  ]

@NgModule({
  declarations: [
    AppComponent,
    // UsersComponent,
    // UserComponent,
    SpinnerComponent,
    // PostsComponent,
    PageNotFoundComponent,
    InvoiceFormComponent,
    InvoicesComponent,
    InvoiceComponent,
    // UserFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    AngularFireModule.initializeApp(firebaseConfig),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [InvoicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
