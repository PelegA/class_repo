import { Component, OnInit } from '@angular/core';
import {InvoicesService} from './invoices.service';
@Component({
  selector: 'my-invoices',
  templateUrl: './invoices.component.html',
  styles: [`
         .posts li { cursor: default; }
         .posts li:hover { background: #ecf0f1; }
         .list-group-item.active, 
         .list-group-item.active:hover { 
           background-color: #ecf0f1;
           border-color: #ecf0f1; 
           color: #2c3e50;
          }
    `]
})
export class InvoicesComponent implements OnInit {
  invoices;
  currentInvoice;
   isLoading:boolean=true;

  select(invoice){
   this.currentInvoice=invoice;
 }

  constructor(private _invoiceService:InvoicesService) { }

   addInvoice(invoice){
    this._invoiceService.addInvoice(invoice);
}
 ngOnInit() {
     this._invoiceService.getInvoices().subscribe(invoicesData=>
     {
       this.invoices=invoicesData;
       this.isLoading=false;
      console.log(this.invoices);
     });
   }

}
