import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';
@Injectable()
export class InvoicesService {
   invoicesObservable;
  constructor(private af:AngularFire) { }

  getInvoices(){
    this.invoicesObservable= this.af.database.list('/invoices').delay(1000);
    return this.invoicesObservable;
  }
  addInvoice(invoice){ 
    this.invoicesObservable.push(invoice); 
  }
}
