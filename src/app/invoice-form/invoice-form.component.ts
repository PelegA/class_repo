import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Invoice} from '../invoice/invoice';
import { NgForm } from '@angular/forms'

@Component({
  selector: 'my-invoice-form',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.css']
})
export class InvoiceFormComponent implements OnInit {
  @Output() invoiceAddedEvent = new EventEmitter<Invoice>();
  invoice:Invoice= {name:'', amount:''};
  constructor() { }

  ngOnInit() {
     console.log(this.invoice.name,this.invoice.amount)
  }
  onSubmit(form:NgForm){
    console.log(form)
    this.invoiceAddedEvent.emit(this.invoice)
// Add a comment to this line
    this.invoice={
      name:'',
      amount:''
    }
  }

}
